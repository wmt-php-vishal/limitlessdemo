<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ComponentController extends Controller
{
    public function basicInput()
    {

        return view('basicInput');

    }

    public function radioCheckbox()
    {

        return view('radioCheckbox');

    }
}
