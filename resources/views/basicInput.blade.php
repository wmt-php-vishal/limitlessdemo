@extends('html')

    @section('js')
        <!-- Theme JS files -->
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/form_inputs.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/ripple.min.js') }}"></script>
<!-- /theme JS files -->
@endsection

@section('title' , 'Basic Inputs')

@section('content')

    <h3>Basic Input View</h3>

    <div class="panel-body">

        <form class="form-horizontal" action="#">
            <fieldset class="content-group">
                <legend class="text-bold">Basic inputs</legend>

                <div class="form-group">
                    <label class="control-label col-lg-2">Default text input</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Password</label>
                    <div class="col-lg-10">
                        <input type="password" class="form-control">
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-lg-2">Static text</label>
                    <div class="col-lg-10">
                        <div class="form-control-static">This is a static text</div>
                    </div>
                </div>




                <div class="form-group">
                    <label class="control-label col-lg-2">Textarea</label>
                    <div class="col-lg-10">
                        <textarea rows="5" cols="5" class="form-control" placeholder="Textarea Placeholder"></textarea>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>


@endsection
